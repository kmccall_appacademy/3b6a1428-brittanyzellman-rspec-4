class Book
  # TODO: your code goes here!
  # attr_reader :title

  def title=(name)
    @title = name
  end

  def title
    arr = @title.split(" ")
    non_capped_words = "a an and the is in at of"
    arr.map! do |word|
      if !non_capped_words.include?(word) || word == "i" ||
         arr.index(word) == 0
        word.capitalize
      else
        word
      end
    end
    arr.join(" ")
  end
end
