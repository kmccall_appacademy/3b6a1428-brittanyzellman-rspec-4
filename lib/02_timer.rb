require 'byebug'

class Timer
  attr_reader :seconds

  def initialize
    @seconds = 0
  end

  def seconds=(seconds)
    @seconds = seconds
  end

  def time_string
    second = @seconds % 60
    minute = (@seconds / 60)
    if minute >= 60
      hour = minute / 60
      minute = minute % 60
    end
    "#{add_zero(hour)}:#{add_zero(minute)}:#{add_zero(second)}"
  end

  def add_zero(num)

    num = 0 if num.nil?
    if num < 10
      "0#{num}"
    else
      num
    end
  end

end
