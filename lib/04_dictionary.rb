require 'byebug'
class Dictionary
  # TODO: your code goes here!
  attr_reader :dict_hash, :key, :def

  def initialize
    @dict_hash = {}
  end

  def add(str)
    str.class == Hash ? @dict_hash = @dict_hash.merge(str) : @dict_hash[str] = nil
  end

  def entries(opt = {})
    @dict_hash = @dict_hash.merge(opt)
  end

  def keywords
    @dict_hash.keys.sort
  end

  def find(str)
    ret_hash = {}
    @dict_hash.each do |k, v|
      if k.include?(str)
        ret_hash[k] = v
      end
    end
    ret_hash
  end

  def printable
    # they want a Regex!!!
    str = []
    @dict_hash.each do |k, v|
      str.unshift("\"#{v}\"\n")
      str.unshift("[#{k}] ")
    end
    s = str[-1]
    s = s.chars
    s.delete(s[-1])
    str[-1] = s
    str.join
  end

  def include?(str)
    return true if keywords.include?(str)
    false
  end

end
