require 'byebug'
class Temperature
  # TODO: your code goes here!
  attr_reader :c, :f

  def initialize(option = {})
    # debugger
    default = {:c => nil, :f => nil}.merge(option)
    @c = default[:c]
    @f = default[:f]
  end

  def in_celsius
    if self.c == nil
      (self.f - 32) * (5.0 / 9.0)
    else
      self.c
    end
  end

  def in_fahrenheit
    if self.f == nil
      (self.c.to_f * 1.8) + 32
    else
      self.f
    end
  end

  def self.from_celsius(num)
    Temperature.new(:c => num)
  end

  def self.from_fahrenheit(num)
    Temperature.new(:f => num)
  end

end

class Celsius < Temperature
  attr_reader :c, :f

  def initialize(num)
    @c = num
  end

end

class Fahrenheit < Temperature
  attr_reader :c, :f

  def initialize(num)
    @f = num
  end

end 
